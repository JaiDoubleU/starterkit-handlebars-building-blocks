# Patternlab Starterkit - Basic Building Blocks

Parts borrowed from <https://github.com/pattern-lab/starterkit-mustache-demo>

This Patternlab starterkit contains the following elements:

* color scheme previews
* font families, including Benton Sans, Proxima Nova, and Font Awesome
* standard HTML tag previews

## Requirements

This Patternlab starterkit requires the following components:

Node:

* `@pattern-lab/engine-handlebars`: [npm](https://www.npmjs.com/package/@pattern-lab/engine-handlebars), [Github](https://github.com/pattern-lab/patternlab-node/tree/master/packages/engine-handlebars)

## Install

```bash
npm install --save-dev @JaiDoubleU/starterkit-handlebars-building-blocks
npx @pattern-lab/cli install --starterkits @JaiDoubleU/starterkit-handlebars-building-blocks
```

## Edit Files

After installation the files of this Starterkit can be found in `source/`.

## Example

You can see a live example of the original repo in action, which this starterkit was forked from, in the url below.  Thanks @ringods for your hard work.

* [zoho-sites-elate-template](https://gitlab.com/ringods/zoho-sites-elate-template)

Submit a merge request and add your project to the list if you want to offer it as a demo project.
